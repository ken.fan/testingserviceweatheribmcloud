package com.mainservice.testingservice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Base64;
import java.nio.charset.StandardCharsets;

@RestController
public class HelloWorldController {

	@Value("${cloudUserName}")
	private String cloudUserName;

	@Value("${cloudPassword}")
	private String cloudPassword;

    @RequestMapping("/location")
    public String inquiryData() {
        HttpURLConnection connection = null;
        InputStream is = null;
		DataOutputStream wr = null;
		BufferedReader rd = null;

		try{
			URL url = new URL("https://twcservice.mybluemix.net/api/weather/v1/location/30339:4:US/forecast/daily/5day.json");

			connection = (HttpURLConnection) url.openConnection();
			String encoded = Base64.getEncoder().encodeToString((cloudUserName+":"+cloudPassword).getBytes(StandardCharsets.UTF_8));
			connection.setRequestProperty("Authorization", "Basic "+encoded);
			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			System.out.println("Response Code received : " + responseCode);

			is = connection.getInputStream();

			rd = new BufferedReader(new InputStreamReader(is));
			StringBuilder response = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();
		}catch(Exception e){
			return "error" + e.toString();
		}finally{
			if (connection != null) {
				connection.disconnect();
			}
			safeClose(is, wr, rd);
		}
    }

    private void safeClose(InputStream is, DataOutputStream wr, BufferedReader rd){
		if(is != null){
    	   try {
    		   is.close();
			} catch (IOException e) {
				//logger.error("Error close connection is "+e);
			}
		}

       if(wr != null){
    	   try {
    		   wr.close();
			} catch (IOException e) {
				//logger.error("Error close connection wr "+e);
			}
       }

       if(rd != null){
    	   try {
    		   rd.close();
			} catch (IOException e) {
				//logger.error("Error close connection wr "+e);
			}
       }
	}

}
