package com.mainservice.testingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestingserviceApplication.class, args);
	}
}
